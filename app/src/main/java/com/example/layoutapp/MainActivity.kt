package com.example.layoutapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val relativeLayout = findViewById<Button>(R.id.btn_relativeLayout)
        relativeLayout.setOnClickListener {
            val intent = Intent(this,RelativeLayoutActivity::class.java)
            startActivity(intent)
        }
    }
}